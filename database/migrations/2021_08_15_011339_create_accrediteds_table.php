<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccreditedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accrediteds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cnpj', 18)->unique();
            $table->string('email')->unique();
            $table->string('corporate_name');
            $table->string('state_registration');
            $table->text('phone');
            $table->longText('address');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accrediteds');
    }
}
