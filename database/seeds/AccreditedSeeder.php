<?php

use Illuminate\Database\Seeder;
use App\Accredited;
class AccreditedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Accredited::create([
            'cnpj' => '01.055.552/0001-69',
            'corporate_name' => 'Exemplo de Credenciada',
            'state_registration' => '10587562',
            'email' => 'yuwq@gmail.com',
            'phone' => '963255265',
            'address' => 'Trav anísio chagas, 234, centro, santarém, pará, brasil'
        ]);
    }
}
