<?php

use App\Role;
use Illuminate\Database\Seeder;
use \App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::where('name', 'admin')->first();
        $manager = Role::where('name', 'manager')->first();
        $employer = Role::where('name', 'employer')->first();
        $user = new User([
            'name' => 'Davi Rodrigues',
            'email' => 'davilimarodrigues@gmail.com',
            'password' => 'petwatcher',
        ]);
        $user_2 = new User([
            'name' => 'Yure Dev',
            'email' => 'yuresamarone34@gmail.com',
            'password' => 'deUmAOito',
        ]);
        $user_3 = new User([
            'name' => 'Debora',
            'email' => 'debora@gmail.com',
            'password' => 'petwatcher',
        ]);
        $admin->users()->save($user);
        $manager->users()->save($user_2);
        $employer->users()->save($user_3);
        $user->save();
        $user_2->save();
        $user_3->save();
    }
}
