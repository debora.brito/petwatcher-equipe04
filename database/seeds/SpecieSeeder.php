<?php

use App\Specie;
use Illuminate\Database\Seeder;

class SpecieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Specie::create([
                'name' => 'Espécie 2',
                'description' => 'Descrição da espécie 2.'
        ]);
        Specie::create([
            'name' => 'Espécie 1',
            'description' => 'Descrição da espécie 1.'
        ]);
    }
}
