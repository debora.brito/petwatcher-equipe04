<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('profile.index');
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'username' => 'required|min:10',
            'password' => 'required|min:8|password'
        ], [
            'email.email' => 'Email Inválido',
            'name.required' => 'Nome necessário!',
            'password.required' => 'Senha Obrigatória',
            'password.password' => 'A Senha está incorreta!'
        ]);
        if ($validator->fails()) {
            return redirect('user/profile')
                ->withErrors($validator)
                ->withInput();
        }
        $user_updated = $request->only('username', 'email', 'password');
        $user = Auth::user();
        $user_id = $user->id;
        $user = User::find($user_id);
        $user->name = $user_updated['username'];
        $user->email = $user_updated['email'];
        if ($user->save()) {
            return redirect('user/profile')->with('success', 'Alterado com sucesso');
        } else {
            return redirect('user/profile')->withErrors(['saving_error' => 'Erro ao salvar']);
        }
    }
    public function show_reset_password()
    {
        return view('profile.passwords.reset');
    }

    public function reset_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:8|password',
            'new_password' =>'required|min:8',
            'password_confirmation' => 'required|min:8|same:new_password' // senha de confirmação precisa ser obrigatória,
                                                                          //no mínimo 8 caracteres e ser a mesma que new_password
        ], [
            'old_password.required' => 'Senha Obrigatória.',
            'old_password.password' => 'A Senha está incorreta!',
            'password_confirmation.required' => 'Senha de confirmação obrigatória.',
            'password_confirmation.same' => 'Senha de confirmação não coincide com a com a senha nova.',
            'new_password.min' => 'A senha precisa ter no mínino 8 caracteres!',
            'password_confirmation.min' => 'A senha precisa ter no mínino 8 caracteres!'
        ]);
        if ($validator->fails()) {
            return redirect('user/resetpassword')
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $password_fields = $request->only('old_password', 'new_password', 'password_confirmation');
        $user_id = $user->id;
        $user = User::find($user_id);
        $user->setPasswordAttribute($password_fields['new_password']);
        if ($user->save()) {
            return redirect('user/profile')->with('success', 'Senha Alterada com sucesso');
        } else {
            return redirect('user/profile')->withErrors(['saving_error' => 'Erro ao salvar']);
        }
    }
}
