<?php

namespace App\Http\Controllers;

use App\Licenca;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

class LicencaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $licencas = Licenca::all();
        return view('licenca.index', compact('licencas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('licenca.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $licenca = new Licenca();
        $licenca->cnpj = $request->cnpj;
        $licenca->licenciamento = $request->licenciamento;
        $licenca->vencimento = $request->vencimento;

        $validacao = Validator::make($request->all(), [
            'cnpj' => 'required|cnpj',
            'licenciamento' => 'required',
            'vencimento' => 'required|date|after:licenciamento'
        ]);

        if ($validacao->fails()) {
            return redirect('licenca/create')->withErrors($validacao)->withInput();
        }
        $licenca->estado = true;
        $licenca->save();
        return redirect('licenca');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Licenca  $licenca
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $licenca = Licenca::findOrFail($id);
        return view('licenca.show', compact('licenca'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Licenca  $licenca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $licenca = Licenca::findOrFail($id);
        return view('licenca.edit', compact('licenca'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Licenca  $licenca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $licenca = Licenca::findOrFail($id);
        $licenca->cnpj = $request->cnpj;
        $licenca->licenciamento = $request->licenciamento;
        $licenca->vencimento = $request->vencimento;
        if ($request->estado == null) {
            $licenca->estado = false;
        } else {
            $licenca->estado = true;
        }
        $licenca->save();
        return redirect('licenca');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Licenca  $licenca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $licenca = Licenca::findOrFail($id);
        $licenca->delete();
        return redirect('licenca');
    }

    public function listarLicencas(Request $request)
    {
        $licenca = Licenca::all();
        $datas_vencimento = array();
        $licencas = Licenca::all();
        foreach ($licencas as $licenca) {
            if ($licenca->cnpj == $request->cnpj && $licenca->estado == true) {
                $datas_vencimento[] = $licenca->vencimento;
                $data = Carbon::createFromDate($licenca->vencimento);
                if ($data->isPast(Carbon::today())) {
                    $datas_vencimento[] = $licenca->vencimento;
                }
            }
        }
        return view('revogacao.index', compact('datas_vencimento', 'licenca'));
    }

    public function revogarLicenca($id)
    {
        $licenca = Licenca::findOrFail($id);
        $licenca->estado = false;
        $licenca->save();
        return redirect('revogarLicenca');
    }
}
