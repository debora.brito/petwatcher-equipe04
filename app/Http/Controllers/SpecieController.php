<?php

namespace App\Http\Controllers;

use App\Specie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SpecieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth','roles:admin'])->except(['show']);
    }
    public function index()
    {
        $species = Specie::all();
        return view('specie.index', compact('species'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('specie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|min:5'
        ]);
        if($validator->fails()){
            return redirect('/specie/create')
                    ->withErrors($validator)
                    ->withInput();
        }
        $specie_input = $request->only('name', 'description');
        $specie = new Specie([
            'name' => $specie_input['name'],
            'description' => $specie_input['description']
        ]);
        if($specie->save()){
            return redirect('/specie')->with('success', 'Espécie cadastrada com sucesso.');
        } else {
            return redirect('/specie/create')->withErrors(['saving_error' => 'Erro ao tentar salvar.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $specie = Specie::findOrFail($id);
        return view('specie.show', compact('specie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specie = Specie::findOrFail($id);
        return view('specie.edit', compact('specie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|min:8'
        ]);
        $specie = Specie::findOrFail($id);
        if($validator->fails()){
            return redirect('/specie/edit')
                    ->withErrors($validator)
                    ->withInput();
        }
        $specie = Specie::findOrFail($id);
        $specie->name = $request->input('name');
        $specie->description = $request->input('description');
        if($specie->save()){
            return redirect('/specie')->with('success', 'Espécie cadastrada com sucesso.');
        } else {
            return redirect("/specie/{$id}/edit")->withErrors(['saving_error' => 'Erro ao tentar salvar.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specie = Specie::findOrFail($id);
        if($specie->delete())
            return redirect('/specie')->with('success', 'Deletado com Sucesso.');
        else
            return redirect('/specie')->withErrors(['saving_error' => 'Erro ao tentar deletar a espécie '.$specie->name.'.']);
    }
}
