<?php

namespace App\Http\Controllers;

use App\Accredited;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AccreditedController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','roles:admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accrediteds = Accredited::all();
        return view('accredited.index', compact('accrediteds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accredited.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cnpj' => 'required|min:18|max:18|regex:/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/|unique:accrediteds,cnpj',
            'corporate_name' => 'required',
            'state_registration' => 'required|numeric',
            'phone' => 'required|min:9',
            'address' => 'required|min:10',
            'email' => 'email:rfc|unique:accrediteds,email'
        ]);
        if($validator->fails()){
            return redirect('/accredited/create')
                    ->withErrors($validator)
                    ->withInput();
        }

        $accredited = new Accredited([
            'cnpj' => $request->input('cnpj'),
            'corporate_name' => $request->input('corporate_name'),
            'state_registration' => $request->input('state_registration'),
            'phone' => $request->input('phone'),
            'address' => $request->input('address'),
            'email' => $request->input('email')

        ]);
        if($accredited->save()){
            return redirect('/accredited')->with('success', 'Credenciada cadastrada com sucesso.');
        } else {
            return redirect('/accredited')->withErrors(['saving_error' => 'Credenciada não foi salva devido a um erro.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $accredited = Accredited::findOrFail($id);
        return view('accredited.show', compact('accredited'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accredited = Accredited::findOrFail($id);
        return view('accredited.edit', compact('accredited'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'cnpj' => 'required|min:18|max:18|regex:/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/',
            'corporate_name' => 'required',
            'state_registration' => 'required|numeric',
            'phone' => 'required|min:9',
            'address' => 'required|min:10',
            'email' => 'email:rfc'
        ]);
        if($validator->fails()){
            return redirect("/accredited/{$id}/edit")
                    ->withErrors($validator)
                    ->withInput();
        }
        $accredited = Accredited::findOrFail($id);
        $accredited_updated = $request->only('cnpj', 'corporate_name', 'state_registration', 'phone', 'address', 'email' );

        $accredited->cnpj = $accredited_updated['cnpj'];
        $accredited->corporate_name = $accredited_updated['corporate_name'];
        $accredited->state_registration = $accredited_updated['state_registration'];
        $accredited->phone = $accredited_updated['phone'];
        $accredited->address= $accredited_updated['address'];
        $accredited->email = $accredited_updated['email'];

        if($accredited->save()){
            return redirect('/accredited')->with('success', 'Credenciada Editada com sucesso.');
        } else {
            return redirect('/accredited')->withErrors(['saving_error' => 'Credenciada não foi salva devido a um erro.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $accredited = Accredited::findOrFail($id);
        if($accredited->delete()){
            return redirect('/accredited')->with('success', 'Deletado com sucesso.');
        } else {
            return redirect('/accredited')->withErrors(['savinf_error' => 'Error ao deletar.']);
        }
    }
}
