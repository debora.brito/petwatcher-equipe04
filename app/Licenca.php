<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licenca extends Model
{
    protected $table = 'licencas';
    use SoftDeletes;
}
