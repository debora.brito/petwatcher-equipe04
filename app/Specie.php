<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specie extends Model
{
    protected $table = 'species';
    protected $fillable = ['name', 'description'];
}
