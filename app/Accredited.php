<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Accredited extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'cnpj', 'email', 'corporate_name', 'state_registration', 'phone', 'address'
    ];
}
