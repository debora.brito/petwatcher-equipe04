<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('testeLogin', function () {
    return view('testeLogin');
});
Route::get('licenca', 'LicencaController@index');
// Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'Auth\LoginController@login');
// Route::get('logout', 'Auth\LoginController@logout')->name('logout');
// Route::get('testeLogin', function () {
//     return view('testeLogin');
// });

Auth::routes(['register' => false]);



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/resetpassword', 'ProfileController@show_reset_password')->name('profile.password.reset');
Route::put('/user/resetpassword', 'ProfileController@reset_password')->name('profile.password.update');
Route::get('/user/profile', 'ProfileController@index')->name('profile.index');
Route::put('/user/profile', 'ProfileController@update')->name('profile.update');
Route::resource('/accredited', 'AccreditedController');
Route::resource('/specie', 'SpecieController');

Route::get('licenca', 'LicencaController@index')->name('licenca.index');
Route::get('licenca/create', 'LicencaController@create');
Route::post('licenca/store', 'LicencaController@store');
Route::get('licenca/{id}', 'LicencaController@show');
Route::put('licenca/{id}', 'LicencaController@update');
Route::delete('licenca/{id}', 'LicencaController@destroy');
Route::get('licenca/{id}/edit', 'LicencaController@edit');

Route::get('revogarLicenca', function () {
    return view('revogacao.index');
})->name('revogacao');
Route::post('revogarLicenca/lista', 'LicencaController@listarLicencas');
Route::post('revogarLicenca/lista/{id}', 'LicencaController@revogarLicenca');
Route::post('revogarLicenca/lista', 'LicencaController@listarLicencas');
Route::post('revogarLicenca/lista/{id}', 'LicencaController@revogarLicenca');
Route::post('revogarLicenca/lista', 'LicencaController@listarLicencas');
Route::post('revogarLicenca/lista/{id}', 'LicencaController@revogarLicenca');
