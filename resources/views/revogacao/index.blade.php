
<div>
    Revogar Licença
</div>

<div>
    <form action="{{ action('LicencaController@listarLicencas') }}" method="post">
        @csrf
        <label for="cnpj">Insira o CNPJ:</label>
        <input type="text" name="cnpj" id="cnpj">

        <input type="submit" value="Enviar">
    </form>
@if($datas_vencimento ?? '')
	@foreach($datas_vencimento as $data)
		<p>
			{{ $data }}
			<form action="{{ action('LicencaController@revogarLicenca', $licenca->id) }}" method="post">
                @csrf
                <a href="#" onclick="if (confirm('Revogar &quot;{{ $licenca->id }}&quot;?')) this.parentNode.submit();">Revogar</a>
            </form>
		</p>
	@endforeach
@endif
</div>
@extends('layouts.app')
@section('content')

<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-3 ">
             <div class="list-group ">
              <a href="#" class="list-group-item list-group-item-action ">{{ __('Perfil')}}</a>
              @if(Auth::user()->isAdmin())
                <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action"> {{__('Gestão de Credenciada')}} </a>
                <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
                <a href="{{ route('licenca.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Licenças') }}</a>
                <a href="{{ route('revogacao') }}" class="list-group-item list-group-item-action active ">Revogação de Licenças</a>
              @endif
              <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Revogação de Licença</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ action('LicencaController@listarLicencas') }}" method="POST">
                                @csrf
                              <div class="form-group row">
                                <label for="cnpj" class="col-4 col-form-label">Insira o CNPJ</label>
                                <div class="col-8">
                                  <input id="cnpj" name="cnpj" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="offset-4 col-4">
                                  <button name="submit" type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                              </div>
                            </form>
                        </div>

                        @if($datas_vencimento ?? '')
                            @foreach($datas_vencimento as $data)
                                <tr>
                                    <td>{{ $data }}</td>
                                    <td>
                                        <form action="{{ action('LicencaController@revogarLicenca', $licenca->id) }}" method="post">
                                            @csrf
                                                <a class="btn btn-danger btn-sm" href="#" onclick="if (confirm('Revogar &quot;{{ $licenca->id}}&quot;?')) this.parentNode.submit();">Revogar</a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
