@extends('layouts.app')
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('success'))
            <div class="alert alert-success">{{ session('success') }} </div>
    @endif
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
              <a href="#" class="list-group-item list-group-item-action active">{{ __('Perfil')}}</a>
              @if(Auth::user()->isAdmin())
                <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action"> {{__('Gestão de Credenciada')}} </a>
                <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
              @endif
              <!--
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
		</div>
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Seu Perfil</h4>
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form action="{{ action('ProfileController@update') }}" method="POST">
                                @csrf
                                @method('PUT')
                              <div class="form-group row">
                                <label for="username" class="col-4 col-form-label">Nome de Usuário*</label>
                                <div class="col-8">
                                  <input id="username" name="username" value="{{Auth::user()->name}}" placeholder="Username" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="select" class="col-4 col-form-label">Papel</label>
                                <div class="col-8">
                                  <select id="select" name="select" disabled class="custom-select">
                                    <option @if(Auth::user()->role_id == 1) {{__('selected')}} @endif value="admin">Administrador</option>
                                    <option @if(Auth::user()->role_id == 2) {{__('selected')}} @endif value="manager">Gerente</option>
                                    <option @if(Auth::user()->role_id == 3) {{__('selected')}} @endif value="employer">Funcionário</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="email" class="col-4 col-form-label">Email*</label>
                                <div class="col-8">
                                  <input id="email" name="email" value={{Auth::user()->email}} placeholder="Email" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                             <div class="form-group row">
                                <label for="password" class="col-4 col-form-label">Senha*</label>
                                <div class="col-8">
                                  <input id="password" name="password" placeholder="Senha" required class="form-control here" type="password">
                                </div>
                              </div>
                              <div class="form-group row">
                                <div class="offset-4 col-4">
                                  <button name="submit" type="submit" class="btn btn-primary">Atualizar meu Perfil</button>
                                </div>
                                <div class="col-4 d-flex align-items-center justify-content-center">
                                    <a href="{{ route('profile.password.reset') }}">Alterar Senha</a>
                                </div>
                              </div>
                            </form>
		                </div>
		            </div>

		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection
