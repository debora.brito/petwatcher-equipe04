@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->role_id === 1)
                        <div class="alert alert-success" role="alert">
                            Olá, Administrador.
                        </div>
                    @endif
                    Dashboard!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
