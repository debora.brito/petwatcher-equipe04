@extends('layouts.app')
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
              <a href="{{route('profile.index')}}" class="list-group-item list-group-item-action ">{{ __('Perfil')}}</a>
              <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action active"> {{__('Gestão de Credenciada')}} </a>
              <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
              <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
		</div>
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Criação de Credenciada</h4>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form action="{{ action('AccreditedController@update', $accredited->id) }}" method="POST">
                                @csrf
                                @method('put')
                                <div class="form-group row">
                                    <label for="corporate_name" class="col-4 col-form-label">Razão Social*</label>
                                    <div class="col-8">
                                      <input id="corporate_name" value="{{$accredited->corporate_name}}"  name="corporate_name" placeholder="Razão Social" class="form-control here"  type="text">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="cnpj" class="col-4 col-form-label">CNPJ*</label>
                                    <div class="col-8">
                                      <input id="cnpj" name="cnpj"  value="{{$accredited->cnpj}}" minlength="18" placeholder="CNPJ" class="form-control here"  type="text">
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Email*</label>
                                    <div class="col-8">
                                      <input id="email" name="email" value="{{$accredited->email}}" placeholder="Email" class="form-control here"  type="email">
                                    </div>
                                  </div>
                                 <div class="form-group row">
                                    <label for="state_registration" class="col-4 col-form-label">Inscrição Estadual*</label>
                                    <div class="col-8">
                                      <input id="state_registration" value="{{$accredited->state_registration}}" name="state_registration" placeholder="Inscrição Estadual"  class="form-control here" type="text">
                                    </div>
                                  </div>
                                 <div class="form-group row">
                                    <label for="phone" class="col-4 col-form-label">Telefone</label>
                                    <div class="col-8">
                                      <input id="phone" name="phone" value="{{$accredited->phone}}" placeholder="Telefone"  class="form-control here" type="tel">
                                    </div>
                                  </div>
                                 <div class="form-group row">
                                    <label for="address" class="col-4 col-form-label">Endereço</label>
                                    <div class="col-8">
                                      <textarea id="address" name="address" placeholder="Endereço" class="form-control here" type="text">{{__($accredited->address)}}</textarea>
                                    </div>
                                 </div>
                              <div class="form-group row">
                                <div class="offset-4 col-4">
                                  <button name="submit" type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                              </div>
                            </form>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection
