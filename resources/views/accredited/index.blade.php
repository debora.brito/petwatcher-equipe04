@extends('layouts.app')
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('success'))
            <div class="alert alert-success">{{ session('success') }} </div>
    @endif
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
              <a href="{{route('profile.index')}}" class="list-group-item list-group-item-action">Perfil</a>
              <a href="#" class="list-group-item list-group-item-action active">Gestão de Credenciada</a>
              <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
              <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
		</div>
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-8">
		                    <h4>Gestão de Credenciada</h4>
		                </div>
                        <div class="col-md-4 d-flex justify-content-center align-items-center">
		                    <a class="btn btn-primary" href="{{route('accredited.create')}}">Adicionar Nova</a>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
                            <table class="table border mt-2">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>CNPJ</th>
                                        <th>Razão Social</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($accrediteds as $accredited)
                                        <tr>
                                            <td>{{ $accredited->id }}</td>
                                            <td>{{ $accredited->cnpj}}</td>
                                            <td>{{ $accredited->corporate_name}}</td>
                                            <td>
                                                <form action="{{ action('AccreditedController@destroy', $accredited->id) }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <a class="btn btn-success btn-sm" href="/accredited/{{ $accredited->id }}">Ver</a>
                                                        <a class="btn btn-primary btn-sm" href="/accredited/{{ $accredited->id }}/edit">Editar</a>
                                                        {{ method_field('delete') }}
                                                        <a class="btn btn-danger btn-sm" href="#" onclick="if (confirm('Apaga &quot;{{ $accredited->corporate_name}}&quot;?')) this.parentNode.submit();">Apagar</a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
		                </div>
		            </div>

		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection
