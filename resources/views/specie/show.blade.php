@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
              <a href="#" class="list-group-item list-group-item-action ">{{ __('Perfil')}}</a>
              @if(Auth::user()->isAdmin())
                <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action"> {{__('Gestão de Credenciada')}} </a>
                <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action active">{{ __('Gestão de Espécies') }}</a>
              @endif
              <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
		</div>
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Informações da Credenciada</h4>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <div>
                              <div class="form-group row">
                                <label for="corporate_name" class="col-4 col-form-label">Razão Social*</label>
                                <div class="col-8">
                                  <input id="corporate_name" value="{{$specie->name}}"  disabled name="name" placeholder="Nome" class="form-control here"  type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="cnpj" class="col-4 col-form-label">CNPJ*</label>
                                <div class="col-8">
                                  <input id="description" name="description" disabled  value="{{$specie->description}}" minlength="18" placeholder="Descrição" class="form-control here"  type="text">
                                </div>
                              </div>
                            </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
@endsection
