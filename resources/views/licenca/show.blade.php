@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="#" class="list-group-item list-group-item-action ">{{ __('Perfil')}}</a>
                @if(Auth::user()->isAdmin())
                <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action"> {{__('Gestão de Credenciada')}} </a>
                <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
                <a href="{{ route('licenca.index') }}" class="list-group-item list-group-item-action active">{{ __('Gestão de Licenças') }}</a>
                <a href="{{ route('revogacao') }}" class="list-group-item list-group-item-action ">Revogação de Licenças</a>
                @endif
                <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Informações da Licença</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="form-group row">
                                    <label for="licenciamento" class="col-4 col-form-label">Data de Licenciamento</label>
                                    <div class="col-8">
                                        <input id="licenciamento" value="{{$licenca->licenciamento}}" disabled name="licenciamento" placeholder="Nome" class="form-control here" type="text">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="vencimento" class="col-4 col-form-label">Data de Vencimento</label>
                                    <div class="col-8">
                                        <input id="vencimento" value="{{$licenca->vencimento}}" disabled name="vencimento" placeholder="Nome" class="form-control here" type="text">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="cnpj" class="col-4 col-form-label">CNPJ*</label>
                                    <div class="col-8">
                                        <input id="cnpj" name="cnpj" disabled value="{{$licenca->cnpj}}" minlength="18" placeholder="Descrição" class="form-control here" type="text">
                                    </div>

                                    @if($licenca->estado==true)
                                    {{ 'Licença Ativa!' }}
                                    @else
                                    {{ 'Licença Inativa!' }}
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection