@extends('layouts.app')
@section('content')
<div class="container">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="row">
        <div class="col-md-3 ">
             <div class="list-group ">
              <a href="{{route('profile.index')}}" class="list-group-item list-group-item-action ">{{ __('Perfil')}}</a>
              <a href="{{ route('accredited.index') }}" class="list-group-item list-group-item-action "> {{__('Gestão de Credenciada')}} </a>
              <a href="{{ route('specie.index') }}" class="list-group-item list-group-item-action">{{ __('Gestão de Espécies') }}</a>
              <a href="{{ route('licenca.index') }}" class="list-group-item list-group-item-action active">{{ __('Gestão de Licenças') }}</a>
              <a href="{{ route('revogacao') }}" class="list-group-item list-group-item-action ">Revogação de Licenças</a>
              <!--
              <a href="#" class="list-group-item list-group-item-action">Used</a>
              <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
              <a href="#" class="list-group-item list-group-item-action">Dealer</a>
              <a href="#" class="list-group-item list-group-item-action">Media</a>
              <a href="#" class="list-group-item list-group-item-action">Post</a>
              <a href="#" class="list-group-item list-group-item-action">Category</a>
              <a href="#" class="list-group-item list-group-item-action">New</a>
              <a href="#" class="list-group-item list-group-item-action">Comments</a>
              <a href="#" class="list-group-item list-group-item-action">Appearance</a>
              <a href="#" class="list-group-item list-group-item-action">Reports</a>
              <a href="#" class="list-group-item list-group-item-action">Settings</a>
              -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Edição de Licença</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ action('LicencaController@update', $licenca->id) }}" method="POST">
                                @method('put')
                                @csrf
                              <div class="form-group row">
                                <label for="cnpj" class="col-4 col-form-label">CNPJ</label>
                                <div class="col-8">
                                  <input id="cnpj" name="cnpj" value="{{ $licenca->cnpj }}" placeholder="CNPJ" class="form-control here" required="required" type="text">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="licenciamento" class="col-4 col-form-label">Data de Licenciamento</label>
                                <div class="col-8">
                                  <input id="licenciamento" name="licenciamento" required class="form-control here" type="date"> Data original: {{ $licenca->licenciamento }}
                                </div>
                              </div>

                              <div class="form-group row">
                                <label for="vencimento" class="col-4 col-form-label">Data de Vencimento</label>
                                <div class="col-8">
                                  <input id="vencimento" name="vencimento" required class="form-control here" type="date">  Data original: {{ $licenca->vencimento }}
                                </div>
                              </div>

                              <div class="form-group row">
                                <div class="offset-4 col-4">
                                  <button name="submit" type="submit" class="btn btn-primary">Enviar</button>
                                </div>
                              </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
